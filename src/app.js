const express = require('express');
const path = require('path');
const logger = require('morgan');
const bodyParser = require('body-parser'); //Con esto se entienden las peticiones POST

const app = express();

const indexRoutes = require('./routes/index');

// settings
app.set('port', process.env.PORT || 3000);
app.set('views',path.join(__dirname,'views'));
app.set('view engine', 'ejs');

//middlewares
app.use(logger('dev'));
app.use(bodyParser.urlencoded({extended: false}));

//routes
app.use('/', indexRoutes); //Cuando entro en la raíz mando la ruta de índice

app.listen(app.get('port'), () => {
	console.log('server on port ', app.get('port'));
});