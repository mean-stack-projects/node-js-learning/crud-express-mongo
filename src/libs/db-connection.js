const mongoose = require('mongoose');

let db;

module.exports = function Connection(){
	if(!db){
		db = mongoose.connect('mongodb://testdb:testdb@ds113636.mlab.com:13636/crud-express-mongo',{
			useMongoClient: true
		});
	}

	return db;
}